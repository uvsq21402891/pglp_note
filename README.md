# PGLP_note

Pour lancer l'application, deux possibilités : java -jar ./target/prise_de_note-1.0-jar-with-dependencies.jar puis entrer une des commandes ou  
java -jar ./target/prise_de_note-1.0-jar-with-dependencies.jar commande.

Commandes possibles : 

	- view ou v + nom de la note
	
	- edit ou e + nom de la note
	
	- delete ou d + nom de la note
	
	- list ou ls
	
	- search ou s + "-a" pour une recherche dans toute la note + req, "-t" pour une recherche de titre + req , "-at" pour recherche par attribut + nom attribut + req.

patterns utilisés :

Le titre est complété dirrectement en début de fichier lors de l'édition d'une nouvelle note.
Si la note contient plus de trois lignes, la date de la note doit se trouver à la troisième ligne sous la forme dd/mm/yyyy.

À chaque édition/suppression d'une nouvelle note, le contenu du fichier index.adoc est examiné, modifié en conséquence et réécrit.
Les fichier d'index par mois, projet et context sont générés en même temps (interface Comparator)


	- Singleton : utilisation d'une enumeration pour App.

	- Command : interface + Une classe par commande spécifique.

	- Factory : afin d'appeler une commande,ainsi que la cr�ation des requ�te de search. permet de respecter le principe DRY .

	- observer : pour notifier des changement au niveau des notes

	- comparator : pour permettre le tri des notes pour les différent index

