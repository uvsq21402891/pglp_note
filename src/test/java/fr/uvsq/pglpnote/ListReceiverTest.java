package fr.uvsq.pglpnote;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ListReceiverTest {

    private ListReceiver list;
    @Before
    public void setup(){
        this.list = new ListReceiver("toto");
    }

    @Test
    public void testListReceiver(){
        assertEquals("toto",list.getRepo());
    }
    @Test(expected = IOException.class)
    public void listTest() throws IOException {
        list.list();
    }
}