package fr.uvsq.pglpnote;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class SearchReceiverTest {

    private SearchReceiver search;
    @Before
    public void setUp(){
        this.search = new SearchReceiver("toto","bla");
    }

    @Test
    public void SearchReceiverTest(){
        assertEquals("toto",search.getRepo());
        assertEquals("bla",search.getReq());
    }
    @Test(expected = IOException.class)
    public void searchTest() throws IOException {
        search.search();
    }
}