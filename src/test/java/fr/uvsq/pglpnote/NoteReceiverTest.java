package fr.uvsq.pglpnote;

import static org.junit.Assert.assertEquals;

import java.io.*;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;


public class NoteReceiverTest {

	private LocalDate ld;
	private NoteReceiver note1;
	private NoteReceiver noteErr;
	private Config config;


	
	@Before()
	public void setUp() {
		config = new Config("note_conf.txt");
		config.setRepo((String) System.getProperties().get("user.dir")+"\\");
		ld = LocalDate.parse("1970-01-01", DateTimeFormatter.ISO_DATE);
		note1 = new NoteReceiver("titre1", "projet1", "context1", ld, config);
		noteErr = new NoteReceiver("existePas",config);
		

	}


	/**
	 * Teste la création d'une noteReceiver
	 */
	@Test()
	public void testNoteReceiver() throws ParseException {
		assertEquals(note1.titre, "titre1"); 
		assertEquals(note1.projet, "projet1");
		assertEquals(note1.context, "context1");
		assertEquals(note1.date, ld);
	}


	@Test(expected = IOException.class)
	public void testView() throws IOException {
		noteErr.view();
	}

	@Test(expected = IOException.class)
	public void testDelete() throws IOException {

		noteErr.delete();
	}

}
