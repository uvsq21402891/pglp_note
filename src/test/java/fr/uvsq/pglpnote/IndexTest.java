package fr.uvsq.pglpnote;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;




/**
 * Classe de test pour les méthode index
 */
public class IndexTest {

	private LocalDate ld;
	private Config config;
	private List<Observer> observers;
	private NoteReceiver note1;
	private Index index;

	@Before()
	public void setUp() throws IOException {
		config = new Config("note_conf.txt");
		ld = LocalDate.parse("1970-01-01", DateTimeFormatter.ISO_DATE);
		note1 = new NoteReceiver("titre1", "projet1", "context1", ld, config);
		index = new Index(config, note1);	
		//copie les fichiers d'index
		if (Files.exists(Paths.get(config.getRepo() + index.indexName))) {
			Files.copy(Paths.get(config.getRepo() + index.indexName), Paths.get(config.getRepo() + "index_test.adoc"),  StandardCopyOption.REPLACE_EXISTING);
		}
		if (Files.exists(Paths.get(config.getRepo() + index.indexByProjectName))) {
			Files.copy(Paths.get(config.getRepo() + index.indexByProjectName), Paths.get(config.getRepo() + "indexByProject_test.adoc"),  StandardCopyOption.REPLACE_EXISTING);
		}
		if (Files.exists(Paths.get(config.getRepo() + index.indexByContextName))) {
			Files.copy(Paths.get(config.getRepo() + index.indexByContextName), Paths.get(config.getRepo() + "indexByContext_test.adoc"),  StandardCopyOption.REPLACE_EXISTING);
		}
		if (Files.exists(Paths.get(config.getRepo() + index.indexByMonthName))) {
			Files.copy(Paths.get(config.getRepo() + index.indexByMonthName), Paths.get(config.getRepo() + "indexByMonth_test.adoc"),  StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	/**
	 * Test d'écriture de fichier via la méthode write de Index
	 */
	@Test()
	public void writeTest() {
		index.write("filename");
		assertTrue(Files.exists(Paths.get(config.getRepo() + "filename")));
	}


	/**
	 * Test de suppresion de fichier d'index via la méthode dédiée de Index
	 */
	@Test()
	public void removeTxtIndexTest() {
		index.removeTxt();
		assertTrue(!Files.exists(Paths.get(config.getRepo() + index.indexName)));
	}

	/**
	 * Test de suppresion de fichier d'index  par mois via la méthode dédiée de Index
	 */
	@Test()
	public void removeTxtIndexByMonthTest() {
		index.removeTxt();
		assertTrue(!Files.exists(Paths.get(config.getRepo() + index.indexByMonthName)));
	}

	/**
	 * Test de suppresion de fichier d'index par contexte via la méthode dédiée de Index
	 */
	@Test()
	public void removeTxtIndexByContextTest() {
		index.removeTxt();
		assertTrue(!Files.exists(Paths.get(config.getRepo() + index.indexByContextName)));
	}

	/**
	 * Test de suppresion de fichier d'index par projet via la méthode dédiée de Index
	 */
	@Test()
	public void removeTxtIndexByProjectTest() {
		index.removeTxt();
		assertTrue(!Files.exists(Paths.get(config.getRepo() + index.indexByProjectName)));
	}


	/*
	 * Test de suppresion d'une note dans l'index
	 */
	/*
	@Test()
	public void deleteIndexTest() throws IOException {
		ld = LocalDate.parse("1970-01-01", DateTimeFormatter.ISO_DATE);
		NoteReceiver note2 = new NoteReceiver("titre2", "projet2", "context2", ld, config);
		note2.edit();
		note2.delete();
		index.getFromtxt();
		assertTrue(!index.listNote.contains(note2));
	}
	*/

	@After()
	public void deleteFile() throws IOException {
		Files.deleteIfExists(Paths.get(config.getRepo() + "filename"));
		//remise en place des fichiers d'index
		Files.move(Paths.get(config.getRepo() + "index_test.adoc"), Paths.get(config.getRepo() + index.indexName), StandardCopyOption.REPLACE_EXISTING);
		Files.move(Paths.get(config.getRepo() + "indexByProject_test.adoc"), Paths.get(config.getRepo() + index.indexByProjectName), StandardCopyOption.REPLACE_EXISTING);
		Files.move(Paths.get(config.getRepo() + "indexByContext_test.adoc"), Paths.get(config.getRepo() + index.indexByContextName), StandardCopyOption.REPLACE_EXISTING);
		Files.move(Paths.get(config.getRepo() + "indexByMonth_test.adoc"),Paths.get(config.getRepo() + index.indexByMonthName), StandardCopyOption.REPLACE_EXISTING);
	}

}
