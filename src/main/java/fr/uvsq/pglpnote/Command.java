package fr.uvsq.pglpnote;

import java.io.IOException;

public interface Command  {
    abstract void apply() throws IOException;
}
