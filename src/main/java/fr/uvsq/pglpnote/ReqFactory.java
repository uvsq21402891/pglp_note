package fr.uvsq.pglpnote;

/**
 * classe de création de requete pour la command search
 */
public class ReqFactory {

    public String getReq(String[] liste ){

        String res = "";
        if(liste.length == 3) {
            if (liste[1].contentEquals("-a")) {
                res = liste[2];
            } else if (liste[1].contentEquals("-t")) {
                res = "= " + liste[2];
            }
            else{
                throw new IllegalArgumentException();
            }
        }
        else if (liste.length ==4) {
            if (liste[1].contentEquals("-at")) {
                res = ":"+liste[2]+": "+ liste[3];
            }
            else{
                throw new IllegalArgumentException();
            }
        }

        return res;
    }
}
