package fr.uvsq.pglpnote;

import java.io.IOException;

/**Classe correspondant à la commande de suppression d'une note
 * @author melanie
 *
 */
public class DeleteCommand implements Command {

   private NoteReceiver note;

   public DeleteCommand(NoteReceiver note) {
      this.note = note;
   }

   @Override 
   public void apply() throws IOException{
      note.delete();
   }
}
