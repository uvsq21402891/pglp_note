package fr.uvsq.pglpnote;


import java.io.File;
import java.io.IOException;

/**
 * classe permettant d'appeler la commande list
 */
public class ListReceiver {

	private String repo;

	public ListReceiver(String repo) {
		this.repo = repo;
	}

	/**
	 * methode permettant de lister le contenu du repertoire des notes
	 * @throws IOException
	 */
	public void list() throws IOException {

		try {
			File file = new File(repo);
			if (!file.exists()){
				throw new IOException();
			}

			String[] files = file.list();
			for(String nom : files){
				System.out.println(nom);
			}
		} catch (IOException e) {
			System.out.println("erreur ouverture dossier");
			throw new IOException();
		}
	}

	public String getRepo() {
		return repo;
	}
}
