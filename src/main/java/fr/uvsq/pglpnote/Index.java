package fr.uvsq.pglpnote;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;



/**
 * Classe Index pour le maintien d'un index
 *
 */
public class Index extends Observer {

	public List<NoteReceiver> listNote = new ArrayList<>();
	public Config config;
	public static String indexName = "index/index.adoc";
	public static String indexByProjectName = "index/indexByProject.adoc";
	public static String indexByContextName = "index/indexByContext.adoc";
	public static String indexByMonthName = "index/indexByMonth.adoc";

	public Index(Config config, NoteReceiver note) {
		this.config = config;
		this.note = note;
		this.note.attach(this);
	}

	/** crée le fichier de nom fileName
	 * @throws IOException
	 */
	public void createIndexFile(String fileName) throws IOException {
		Files.createFile(Paths.get(config.getRepo() + fileName));
	}

	/**
	 * Supprime tous les fichiers d'index 
	 */
	public void removeTxt() {
		try {
			Files.deleteIfExists(Paths.get(config.getRepo() + indexName));
			Files.deleteIfExists(Paths.get(config.getRepo() + indexByProjectName));
			Files.deleteIfExists(Paths.get(config.getRepo() + indexByContextName));
			Files.deleteIfExists(Paths.get(config.getRepo() + indexByMonthName));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Si le fichier index.txt existe, lit ce fichier stocke les noteReceiver correspondantes dans la liste de Index
	 * puis supprime le fichier 
	 */
	public void getFromtxt() {
		try {
			List<String> lines = new ArrayList();

			if ( Files.exists(Paths.get(config.getRepo() + indexName))) {
				lines = Files.readAllLines(Paths.get(config.getRepo() + indexName));
				for(String str : lines) {

					List<String> listStr = new ArrayList(Arrays.asList(str.split(" ")));
					LocalDate ldt = LocalDate.parse(listStr.get(1), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
					NoteReceiver note = new NoteReceiver(listStr.get(0), listStr.get(2), listStr.get(3), ldt, this.config);
					insert(note);
				}
				removeTxt();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Écrit la liste des notes dans le fichier fileName en le créant s'il n'existe pas
	 */
	public void write(String fileName){
		if(Files.notExists(Paths.get(config.getRepo() + fileName))) {
			try {
				createIndexFile(fileName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(NoteReceiver note: this.listNote) {
			List<String> line = new ArrayList<>();
			line.add(note.getTitre()+ " " + note.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+ " " + note.getProjet() + " " + note.getContext() + " +");
			try {
				Files.write(Paths.get(config.getRepo() + fileName), line, StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Insert la note dans la liste des notes de l'index
	 * @param note la note à insérer
	 */
	private void insert(NoteReceiver note) {
		this.listNote.add(note);
	}

	/**
	 * Supprime de la liste de Notes de l'index la ligne correspondant à la note
	 * dont le titre est titre si elle existe, ne fait rien sinon
	 * @param note
	 */
	private void remove(NoteReceiver note) {
		listNote.remove(note);
	}



	/**
	 * Met à jour l'index en supprimant la note de la liste de l'index qui porte le même
	 * titre que note et insère la nouvelle note
	 */
	@Override
	public void updateEdit(NoteReceiver note) {
		getFromtxt();
		remove(note);
		insert(note);

		sortByTitre();
		write(indexName);


		sortByProjet();
		write(indexByProjectName);

		sortByContext();
		write(indexByContextName);

		sortByMonth();
		write(indexByMonthName);
	}


	/**
	 * Met à jour l'index en supprimant la note de la liste de l'index qui porte le même
	 * titre que note
	 */
	@Override
	public void updateDelete(NoteReceiver note) {
		getFromtxt();
		remove(note);

		sortByTitre();
		write(indexName);

		sortByProjet();
		write(indexByProjectName);

		sortByContext();
		write(indexByContextName);

		sortByMonth();
		write(indexByMonthName);
	}

	/**
	 * Trie la liste de Notes par titre
	 */
	public void sortByTitre() {
		Collections.sort(listNote, new SortByTitre());
	}

	/**
	 * Trie la liste de Notes par projet
	 */
	public void sortByProjet() {
		Collections.sort(listNote, new SortByProjet());
	}

	/**
	 * Trie la liste de Notes par context
	 */
	public void sortByContext() {
		Collections.sort(listNote, new SortByContext());
	}

	/**
	 * Trie la liste de Notes par mois
	 */
	public void sortByMonth() {
		Collections.sort(listNote, new SortByMonth());
	}

}