package fr.uvsq.pglpnote;


import java.io.IOException;




public class CommandFactory {

	/**
	 * @param listes un tableau de String
	 * @param config le fichier de configuration
	 */
	public Command getCommand(String[] listes, Config config) {
		if(listes.length <= 1){
			if (listes[0].equals("list") || listes[0].equals("ls")){
				return new ListCommand(config.getRepo());
			}
			else {
				throw new IllegalArgumentException();
			}
		}
		else if(listes.length >=3 ){
			if (listes[0].equals("search") || listes[0].equals("s")){
				return new SearchCommand(config.getRepo(),new ReqFactory().getReq(listes));
			}
			else {
				throw new IllegalArgumentException();
			}
		}

		else {
			NoteReceiver note = new NoteReceiver(listes[1],config);

			String nom = listes[1];
			if (listes[0].equals("edit") || listes[0].equals("e")){
				return new EditCommand(note);
			}

			else if (listes[0].equals("view") || listes[0].equals("v")){

				return new ViewCommand(note);
			}
			
			else if (listes[0].equals("delete") || listes[0].equals("d")){

				return new DeleteCommand(note);
			}


			else {
				throw new IllegalArgumentException(); 
			}
		}
		
	
	}
	
}