package fr.uvsq.pglpnote;


/**
 * Classe Pattern Observer pour la mise à jour de l'index
 * @author jc
 *
 */

public abstract class Observer {
	public NoteReceiver note;
	
	public abstract void updateEdit(NoteReceiver note);
	public abstract void updateDelete(NoteReceiver note);

}