package fr.uvsq.pglpnote;
/**Classe correspondant à la commande d'edition
 * @author melanie
 *
 */
public class EditCommand implements Command {

   private NoteReceiver note;

   public EditCommand(NoteReceiver note) {
      this.note = note;
   }

   @Override 
   public void apply(){
      note.edit();
   }
}
