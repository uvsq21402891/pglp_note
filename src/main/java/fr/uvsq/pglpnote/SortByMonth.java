package fr.uvsq.pglpnote;

import java.util.Comparator;

/**
 * Classe permettant de comparer deux notes par date
 */

public class SortByMonth implements Comparator<NoteReceiver>{

	/**
	 * Compare deux notes par années, et à années égales par mois
	 */
	@Override
	public int compare(NoteReceiver note1, NoteReceiver note2) {
		int r = 0;
		if (note1.getDate().getYear() < note2.getDate().getYear()) { r = -1;}
		if (note1.getDate().getYear() > note2.getDate().getYear()) { r = 1;}
		if (note1.getDate().getYear() == note2.getDate().getYear()) {
			r = note1.getDate().getMonthValue() - note2.getDate().getMonthValue();
		}
		return r;
	}
}
