package fr.uvsq.pglpnote;

import java.util.Comparator;

/**
 * Classe permettant de comparer deux notes par context
 */

public class SortByContext implements Comparator<NoteReceiver>{

	/**
	 * Comparer deux notes par ordre alphabétique de context
	 */
	@Override
	public int compare(NoteReceiver note1, NoteReceiver note2) {
		int res = 0;
		if (note1.projet == null && note2.projet == null) { res = 0;}
		if (note1.projet == null && note2.projet != null) { res = -1;}
		if (note1.projet != null && note2.projet == null) { res = 1;}
		if (note1.projet != null && note2.projet != null) {
			res = note1.getContext().compareToIgnoreCase(note2.getContext());
		}
		return res;
	}
}
