package fr.uvsq.pglpnote;

import java.io.BufferedReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


import java.nio.file.StandardOpenOption;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class NoteReceiver {

	public String titre;

	public String projet;
	public LocalDate date;
	public String context;
	private Config config;

	private List<Observer> observers;




	public NoteReceiver(String titre,Config config)  {
		this.titre = titre;
		this.config = config;
		this.observers = new ArrayList<Observer>();
	}
	public NoteReceiver(String titre, String projet, String context, LocalDate date, Config config) {
		this.titre = titre;
		this.projet = projet;
		this.date = date;
		this.config = config;
		this.context = context;
		this.observers = new ArrayList<Observer>();
	}


	public void edit() {
		try {
			if(Files.notExists(Paths.get(config.getRepo() + titre + ".adoc"))) {
				Files.createFile(Paths.get(config.getRepo() + titre + ".adoc"));
				List<String> line1 = new ArrayList<>();
				line1.add("= " + titre);
				line1.add("saisir le nom du propriétaire?");
				Files.write(Paths.get(config.getRepo() + titre + ".adoc"),line1 , StandardOpenOption.APPEND);
				System.out.println("note créer");
				return;
			}
			try {
				Process process = Runtime.getRuntime().exec(config.getEditor() + " " + config.getRepo() + titre + ".adoc");
				process.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//mise à jour du projet l'index
			this.setProjet();
			this.setDate();
			this.setContext();
			//mise à jour de l'index
			new Index(config, this);
			notifyAllObserversEdit();

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}


	public void view() throws IOException {

		if(Files.notExists(Paths.get(config.getRepo()+titre+".adoc"))) {
			throw new IOException();
		}
		Process process = Runtime.getRuntime().exec(config.getBrowser()+" "+config.getRepo()+titre+".adoc");
	}



	public void delete() throws IOException {
		File f = new File(config.getRepo() + titre + ".adoc");
		if(!f.exists())
			throw new IOException();
		Files.delete(Paths.get(config.getRepo() + titre + ".adoc"));

		new Index(config, this);
		notifyAllObserversDelete();
	}




	/**
	 * Notifie aux observateurs les modifications de type edit 
	 */
	public void notifyAllObserversEdit(){
		for (Observer observer : observers) {
			observer.updateEdit(this);
		}
	} 	

	/**
	 * Notifie aux observateurs les modifications de type delete 
	 */
	public void notifyAllObserversDelete(){
		for (Observer observer : observers) {
			observer.updateDelete(this);
		}
	}

	/**
	 * Associe l'obsevateur observer à la note
	 * @param observer
	 */
	public void attach(Observer observer){
		observers.add(observer);
	}


	/**
	 * Définit le projet de la note en allant chercher dans le fichier adoc
	 */
	public void setProjet() {
		try {
			List<String> lines = new ArrayList();
			String nomProjet = "notSet";
			if ( Files.exists(Paths.get(config.getRepo() + titre + ".adoc"))) {
				lines = Files.readAllLines(Paths.get(config.getRepo() + titre + ".adoc"));
			}
			for(String str : lines) {
				if ( str.startsWith(":project:") )  {
					int indiceNomProjet = str.lastIndexOf(":project:") + ":project:".length() + 1 ;
					nomProjet = str.substring(indiceNomProjet);
				}
			}
			this.projet = nomProjet;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Définit la date de la note en lisant la troisième ligne du fichier si cette ligne existe, assigne la date du jour sinon
	 * et l'écrit à la ligne 2
	 * L'utilisateur doit saisir une chaine qui est une date sous la forme "dd/MM/yyyy"  
	 */
	public void setDate() {
		try {
			List<String> lines = new ArrayList<String>();
			LocalDate dateProjet = LocalDate.now();
			if ( Files.exists(Paths.get(config.getRepo() + titre + ".adoc"))) {
				lines = Files.readAllLines(Paths.get(config.getRepo() + titre + ".adoc"));
				if (lines.size() > 2) {
					try {
						dateProjet = LocalDate.parse(lines.get(2), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					if (lines.size() == 1) {
						List<String> line2 = new ArrayList<>();
						line2.add("");
						Files.write(Paths.get(config.getRepo() + titre + ".adoc"),line2, StandardOpenOption.APPEND);//ligne2 si elle n'existe pas	
					}
					List<String> line3 = new ArrayList<>();
					line3.add(dateProjet.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
					Files.write(Paths.get(config.getRepo() + titre + ".adoc"),line3, StandardOpenOption.APPEND);
				}
				this.date = dateProjet;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Définit le projet de la note en allant chercher dans le fichier adoc
	 */
	public void setContext() {
		try {
			List<String> lines = new ArrayList<String>();
			String nomContext = "notSet";
			if ( Files.exists(Paths.get(config.getRepo() + titre + ".adoc"))) {
				lines = Files.readAllLines(Paths.get(config.getRepo() + titre + ".adoc"));
			}
			for(String str : lines) {
				if ( str.startsWith(":context:") )  {
					int indiceNomContext = str.lastIndexOf(":context:") + ":context:".length() + 1 ;
					nomContext = str.substring(indiceNomContext);
				}
			}
			this.context = nomContext;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	/**
	 * Renvoie le titre de la note
	 * @return titre le titre de la note
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Renvoie le projet de la note
	 * @return projet le projet de la note
	 */
	public String getProjet() {
		return projet;
	}

	/**
	 * Renvoie la date de la note
	 * @return date la date de la note
	 */
	public LocalDate getDate() {
		return date;
	}


	/**
	 * Renvoie le context de la note
	 * @return context le context de la note
	 */
	public String getContext() {
		return context;
	}


	public String toString() {
		return "(" + titre + ", " + projet + ", " + context + ", " + date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ")";  
	}



	/**
	 * Redéfinition de la méthode equals pour la gestion de l'index
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoteReceiver other = (NoteReceiver) obj;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}
}