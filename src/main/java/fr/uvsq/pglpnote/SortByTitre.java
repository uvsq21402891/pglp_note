package fr.uvsq.pglpnote;

import java.util.Comparator;

/**
 * Classe permettant de comparer deux notes par titre
 */
public class SortByTitre implements Comparator<NoteReceiver>{

	
	/**
	 * Compare deux notes par ordre alphabétique de titre
	 */
	@Override
	public int compare(NoteReceiver note1, NoteReceiver note2) {
		return note1.titre.compareToIgnoreCase(note2.titre);
	}

}
