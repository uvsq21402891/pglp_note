package fr.uvsq.pglpnote;

import java.io.IOException;

public class SearchCommand implements Command{

    private SearchReceiver search;

    public SearchCommand(String repo, String req) {
        this.search = new SearchReceiver(repo,req);
    }

    @Override
    public void apply() throws IOException {
        search.search();
    }
}
