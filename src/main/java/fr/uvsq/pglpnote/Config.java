package fr.uvsq.pglpnote;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * classe de création du fichier de configuration
 */
public class Config {
	private Properties appConfig;
	private String fileName;

	public Config(String fileName) {
		this.fileName = fileName;
		this.appConfig = new Properties();
		try {
			FileInputStream in = new FileInputStream(fileName);
			appConfig.load(in);
			in.close();
		} catch (IOException e) {
			System.out.println("J'ai pas réussi à ouvrir les propriétés");

		}
	}

	public String getRepo() {
		return appConfig.getProperty("repo", "./");
	}
	
	//e.g : pour demander le path dans windows
	public void setRepo(String repo) {
		appConfig.setProperty("repo", repo);
	}

	public String getBrowser() {
		return appConfig.getProperty("browser", "firefox");
	}
	
	public String getEditor() {
		return appConfig.getProperty("editor", "gedit");
	}
	
	public void save() {
		try {
			FileOutputStream out = new FileOutputStream(fileName);
			appConfig.store(out, "No comment");
			out.close();
		}catch (IOException e) {
			System.out.println("J'ai pas réussi à sauver les propriétés");
		}
	} 

}
