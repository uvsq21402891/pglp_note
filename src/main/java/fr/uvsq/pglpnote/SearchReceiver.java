package fr.uvsq.pglpnote;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * classe permettant d'appeler la command search
 */

public class SearchReceiver {

    private String repo;
    private String req;

    public SearchReceiver(String repo, String req) {
        this.repo = repo;
        this.req = req;
    }

    /**
     * methode listant les notes ou apparaisse la req
     * @throws IOException
     */
    public void search() throws IOException {

        List<String> resExact = new ArrayList();
        List<String> resApro = new ArrayList();

        try {
            File f = new File(repo);
            if (!f.exists()){
                throw new IOException();
            }

            File[] files = f.listFiles();

            for(File nom : files){

                if(nom.isFile()) {
                    BufferedReader br = new BufferedReader(new FileReader(nom));
                    String line;
                    while ((line = br.readLine()) != null) {

                        if (line.contentEquals(req)) {
                            resExact.add(nom.getName());
                        } else if (line.contains(req)) {
                            resApro.add(nom.getName());
                        }
                    }
                    br.close();
                }
            }
        }
        catch (IOException e) {
               System.out.println("erreur ouverture de fichier");
               throw new IOException();
        }


        System.out.println("Resultat exact:");
        for (String s: resExact) {
            System.out.println(s);
        }
        System.out.println("Resultat approximatif:");
        for (String s: resApro) {
            System.out.println(s);
        }
    }

    public String getRepo() {
        return repo;
    }

    public String getReq() {
        return req;
    }
}
