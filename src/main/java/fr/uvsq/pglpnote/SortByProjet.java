package fr.uvsq.pglpnote;

import java.util.Comparator;

/**
 * Classe permettant de comparer deux notes par projet
 */

public class SortByProjet implements Comparator<NoteReceiver>{

	/**
	 *Compare deux notes par ordre alphabétique de projet
	 */
	@Override
	public int compare(NoteReceiver note1, NoteReceiver note2) {
		int res = 0;
		if (note1.projet == null && note2.projet == null) { res = 0;}
		if (note1.projet == null && note2.projet != null) { res = -1;}
		if (note1.projet != null && note2.projet == null) { res = 1;}
		if (note1.projet != null && note2.projet != null) {
			res = note1.projet.compareToIgnoreCase(note2.projet);
		}
		return res;
	}
}
