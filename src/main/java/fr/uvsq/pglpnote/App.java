package fr.uvsq.pglpnote;
import java.io.*;
import java.text.ParseException;
import java.util.Scanner;

// Pattern Singleton
public enum App {
	ENVIRONNEMENT;


	public void run(String[] args, Config config) {
		//Invoker

			String[] listeS = args;
			Scanner sc = new Scanner(System.in);
			String str;

			if (args.length == 0) {
				while (true) {
					str = sc.nextLine();

					if (str.equals("exit")) {
						return;
					}

					listeS = str.split(" ");

					try {
						Command commandNote = new CommandFactory().getCommand(listeS, config);
						commandNote.apply();
					}
					catch (IOException e) {
						System.out.println("erreur ouverture de fichier");
					}
					catch (IllegalArgumentException e) {
						System.out.println("erreur parametre de commande");
					}
				}
			}
			else {

				try {
					Command commandNote = new CommandFactory().getCommand(listeS, config);
					commandNote.apply();
				}
				catch (IOException e) {
					System.out.println("erreur ouverture de fichier");
				}
				catch (IllegalArgumentException e) {
					System.out.println("erreur parametre de commande");
				}
			}
	}

	

	public static void main(String[] args) throws ParseException{
		//pattern singleton
		Config config = new Config("./note_conf.txt");
		ENVIRONNEMENT.run(args, config);

		config.save();
	}

}

