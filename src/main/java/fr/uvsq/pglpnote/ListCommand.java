package fr.uvsq.pglpnote;

import java.io.IOException;

public class ListCommand implements Command {

    private ListReceiver list;

    public ListCommand(String repo) {
        this.list = new ListReceiver(repo);
    }

    @Override
    public void apply() throws IOException {
        list.list();
    }
}
