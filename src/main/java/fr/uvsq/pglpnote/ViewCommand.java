package fr.uvsq.pglpnote;

import java.io.IOException;

/**Classe correspondant à la commande de vue d'une note
 * @author melanie
 *
 */
public class ViewCommand implements Command {

   private NoteReceiver note;

   public ViewCommand(NoteReceiver note) {
      this.note = note;
   }

   @Override
   public void apply() throws IOException {
      note.view();
   }
}
